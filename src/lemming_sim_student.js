// vim: et:sw=2:sts=2
/* Lemmings - robot and GUI script.
 *
 * Copyright 2016 Harmen de Weerd
 * Copyright 2017 Johannes Keyser, James Cooke, George Kachergis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * The file is currently set to run experiment 1 with 1 robot.
 * This is determined by boxFric on line 213:
 * boxFric: 0.000001,    // expiriment 1
 * 
 * For experiment 2 the different conditions are
 * boxFric: 0.0000005,   // expiriment 2
 * boxFric: 0.0000015,   // expiriment 2
 * boxFric: 0.0000020,   // expiriment 2
 *
 * The number of robots can be changed on line 325:
 * setRobotNumber(1); // can be set to 2 or 3 for that many robots
 */


// Description of robot(s), and attached sensor(s) used by InstantiateRobot()
RobotInfo = [
  {body: null,  // for MatterJS body, added by InstantiateRobot()
    color: [255, 0, 0],  // color of the robot shape
    init: {x: 50, y: 50, angle: 0.25 * Math.PI},  // initial position and orientation
    sensors: [  // define an array of sensors on the robot
      // upper distance sensor (walls)
      {
        sense: senseDistanceWalls, // function handle, determines type of sensor
        minVal: 0,                 // minimum detectable distance, in pixels
        maxVal: 20,                // maximum detectable distance, in pixels
        attachAngle: 0,            // sensor's angle on robot body
        attachRadius: 20,          // where the sensor is mounted on robot body
        lookAngle: 0,              // direction the sensor is looking (relative to center-out)
        id: 'distWalls',           // a unique, arbitrary ID of the sensor, for printing/debugging
        color: [150, 0, 0],        // sensor color [in RGB], to distinguish them
        parent: null,              // robot object the sensor is attached to, added by InstantiateRobot
        value: null,               // sensor value, i.e. distance in pixels; updated by sense() function
        valueStr: ''               // sensor value for printing on screen/writing to HTML
      },
      // lower distance sensor (boxes)
      {
        sense: senseDistanceBoxes,
        minVal: 0,
        maxVal: 20,
        attachAngle: 0,
        attachRadius: 20,
        lookAngle: 0,
        id: 'distBoxes',
        color: [0, 150, 0],
        parent: null,
        value: null,
        valueStr: ''
      },
      // colour sensor
      {
        sense: senseColour,
        minVal: 0,
        maxVal: 5,
        attachAngle: 0.1 * Math.PI,
        attachRadius: 0,
        lookAngle: 0,
        id: 'colour',
        color: [0, 0, 150],
        parent: null,
        value: null,
        valueStr: ''
      },
      // define a gyroscope/angle sensor
      {
        sense: senseRobotAngle,
        id: 'gyro',
        parent: null,
        value: null,
        valueStr: ''
      }
    ]
  },
  {body: null,  // for MatterJS body, added by InstantiateRobot()
    color: [0, 0, 255],  // color of the robot shape
    init: {x: 400, y: 400, angle: 1.25 * Math.PI},  // initial position and orientation
    sensors: [  // define an array of sensors on the robot
      // upper distance sensor (walls)
      {
        sense: senseDistanceWalls, // function handle, determines type of sensor
        minVal: 0,                 // minimum detectable distance, in pixels
        maxVal: 25,                // maximum detectable distance, in pixels
        attachAngle: 0,            // sensor's angle on robot body
        attachRadius: 20,          // where the sensor is mounted on robot body
        lookAngle: 0,              // direction the sensor is looking (relative to center-out)
        id: 'distWalls',           // a unique, arbitrary ID of the sensor, for printing/debugging
        color: [150, 0, 0],        // sensor color [in RGB], to distinguish them
        parent: null,              // robot object the sensor is attached to, added by InstantiateRobot
        value: null,               // sensor value, i.e. distance in pixels; updated by sense() function
        valueStr: ''               // sensor value for printing on screen/writing to HTML
      },
      // lower distance sensor (boxes)
      {
        sense: senseDistanceBoxes,
        minVal: 0,
        maxVal: 20,
        attachAngle: 0,
        attachRadius: 20,
        lookAngle: 0,
        id: 'distBoxes',
        color: [0, 150, 0],
        parent: null,
        value: null,
        valueStr: ''
      },
      // colour sensor
      {
        sense: senseColour,
        minVal: 0,
        maxVal: 5,
        attachAngle: 0.1 * Math.PI,
        attachRadius: 0,
        lookAngle: 0,
        id: 'colour',
        color: [0, 0, 150],
        parent: null,
        value: null,
        valueStr: ''
      },
      // define a gyroscope/angle sensor
      {
        sense: senseRobotAngle,
        id: 'gyro',
        parent: null,
        value: null,
        valueStr: ''
      }
    ]
  },
  {body: null,  // for MatterJS body, added by InstantiateRobot()
    color: [0, 255, 0],  // color of the robot shape
    init: {x: 400, y: 50, angle: 0.75 * Math.PI},  // initial position and orientation
    sensors: [  // define an array of sensors on the robot
      // upper distance sensor (walls)
      {
        sense: senseDistanceWalls, // function handle, determines type of sensor
        minVal: 0,                 // minimum detectable distance, in pixels
        maxVal: 25,                // maximum detectable distance, in pixels
        attachAngle: 0,            // sensor's angle on robot body
        attachRadius: 20,          // where the sensor is mounted on robot body
        lookAngle: 0,              // direction the sensor is looking (relative to center-out)
        id: 'distWalls',           // a unique, arbitrary ID of the sensor, for printing/debugging
        color: [150, 0, 0],        // sensor color [in RGB], to distinguish them
        parent: null,              // robot object the sensor is attached to, added by InstantiateRobot
        value: null,               // sensor value, i.e. distance in pixels; updated by sense() function
        valueStr: ''               // sensor value for printing on screen/writing to HTML
      },
      // lower distance sensor (boxes)
      {
        sense: senseDistanceBoxes,
        minVal: 0,
        maxVal: 20,
        attachAngle: 0,
        attachRadius: 20,
        lookAngle: 0,
        id: 'distBoxes',
        color: [0, 150, 0],
        parent: null,
        value: null,
        valueStr: ''
      },
      // colour sensor
      {
        sense: senseColour,
        minVal: 0,
        maxVal: 5,
        attachAngle: 0.1 * Math.PI,
        attachRadius: 0,
        lookAngle: 0,
        id: 'colour',
        color: [0, 0, 150],
        parent: null,
        value: null,
        valueStr: ''
      },
      // define a gyroscope/angle sensor
      {
        sense: senseRobotAngle,
        id: 'gyro',
        parent: null,
        value: null,
        valueStr: ''
      }
    ]
  },
];

// Simulation settings; please change anything that you think makes sense.
simInfo = {
  maxSteps: 60000,      // maximal number of simulation steps to run
  airDrag: 0.1,         // "air" friction of environment; 0 is vacuum, 0.9 is molasses
  boxFric: 0.000001,    // friction between boxes during collisions
  boxMass: 0.01,        // mass of boxes
  boxSize: 10,          // size of the boxes, in pixels
  robotSize: 10,        // approximate robot radius, to select by clicking with mouse
  robotMass: 0.4,       // robot mass (a.u)
  gravity: 0,           // constant acceleration in Y-direction
  bayRobot: null,       // currently selected robot
  baySensor: null,      // currently selected sensor
  bayScale: 3,          // scale within 2nd, inset canvas showing robot in it's "bay"
  doContinue: true,     // whether to continue simulation, set in HTML
  debugSensors: true,   // plot sensor rays and mark detected objects
  debugMouse: true,     // allow dragging any object with the mouse
  engine: null,         // MatterJS 2D physics engine
  world: null,          // world object (composite of all objects in MatterJS engine)
  runner: null,         // object for running MatterJS engine
  height: null,         // set in HTML file; height of arena (world canvas), in pixels
  width: null,          // set in HTML file; width of arena (world canvas), in pixels
  curSteps: 0,          // increased by simStep()
  blueBoxesCloseToWall: 0,
  redBoxesCloseToWall: 0
};

robots = new Array();
sensors = new Array();

// constants and globals for robotMove behaviour
const driveSpeed    = 0.0002,
  wanderTorque      = 0.000008,
  spinTorqueKeep    = 0.0002,
  spinTorqueDump    = 0.0008,
  spinAngleKeep     =  2/3 * Math.PI, // right
  spinAngleDump     = -2/3 * Math.PI; // left

var nrOfBlueBoxes = 0;
var nrOfRedBoxes = 0;

function init() {  // called once when loading HTML file
  const robotBay = document.getElementById("bayLemming"),
    arena = document.getElementById("arenaLemming"),
    height = arena.height,
    width = arena.width;
  simInfo.height = height;
  simInfo.width = width;

  /* Create a MatterJS engine and world. */
  simInfo.engine = Matter.Engine.create();
  simInfo.world = simInfo.engine.world;
  simInfo.world.gravity.y = simInfo.gravity;
  simInfo.engine.timing.timeScale = 1;

  /* Create walls and boxes, and add them to the world. */
  // note that "roles" are custom properties for rendering (not from MatterJS)
  function getWall(x, y, width, height) {
    return Matter.Bodies.rectangle(x, y, width, height,
      {isStatic: true, role: 'wall',
        color:[150, 150, 150]});
  };
  const wall_lo = getWall(width/2, height-5, width-5, 5),
    wall_hi = getWall(width/2, 5, width-5, 5),
    wall_le = getWall(5, height/2, 5, height-15),
    wall_ri = getWall(width-5, height/2, 5, height-15);
  Matter.World.add(simInfo.world, [wall_lo, wall_hi, wall_le, wall_ri]);

  /* Add a bunch of boxes in a neat grid. */
  function getBox(x, y, i, j) {
    if ((i + j) % 2 == 1 ){
      color = [0, 0, 200];
      nrOfBlueBoxes++;
    }
    else {
      color = [200, 0, 0];
      nrOfRedBoxes++;
    }
    box = Matter.Bodies.rectangle(x, y, simInfo.boxSize, simInfo.boxSize,
      {
        frictionAir: simInfo.airDrag,
        friction: simInfo.boxFric,
        mass: simInfo.boxMass,
        role: 'box',
        color: color
      }
    );
    return box;
  };

  const startX = 65, startY = 65,
    nBoxX = 7, nBoxY = 7,
    gapX = 40, gapY = 40,
    stack = Matter.Composites.stack(startX, startY,
      nBoxX, nBoxY,
      gapX, gapY, getBox);
  Matter.World.add(simInfo.world, stack);

  /* Add debugging mouse control for dragging objects. */
  if (simInfo.debugMouse){
    const mouseConstraint = Matter.MouseConstraint.create(simInfo.engine,
      {mouse: Matter.Mouse.create(arena),
        // spring stiffness mouse ~ object
        constraint: {stiffness: 0.5}});
    Matter.World.add(simInfo.world, mouseConstraint);
  }
  // Add the tracker functions from mouse.js
  addMouseTracker(arena);
  addMouseTracker(robotBay);

  /* Running the MatterJS physics engine (without rendering). */
  simInfo.runner = Matter.Runner.create({fps: 60, isFixed: false});
  Matter.Runner.start(simInfo.runner, simInfo.engine);
  // register function simStep() as callback to MatterJS's engine events
  Matter.Events.on(simInfo.engine, 'tick', simStep);

  /* Create robot(s). */
  setRobotNumber(1);  // requires defined simInfo.world
  loadBay(robots[0]);

  /* print the used (interesting parameters to the screen */
  let parameterString = "";
  parameterString += "<tr><td>maxSteps:</td><td>" + simInfo.maxSteps + "</td></tr>";
  parameterString += "<tr><td>airDrag:</td><td>" + simInfo.airDrag + "</td></tr>";
  parameterString += "<tr><td>boxFric:</td><td>" + simInfo.boxFric + "</td></tr>";
  parameterString += "<tr><td>boxMass:</td><td>" + simInfo.boxMass + "</td></tr>";
  parameterString += "<tr><td>boxSize:</td><td>" + simInfo.boxSize + "</td></tr>";
  parameterString += "<tr><td>robotSize:</td><td>" + simInfo.robotSize + "</td></tr>";
  parameterString += "<tr><td>robotMass:</td><td>" + simInfo.robotMass + "</td></tr>";
  parameterString += "<tr><td>gravity:</td><td>" + simInfo.gravity + "</td></tr>";
  parameterString += "<tr><td>bayScale:</td><td>" + simInfo.bayScale + "</td></tr>";
  parameterString += "<tr><td>doContinue:</td><td>" + simInfo.doContinue + "</td></tr>";
  parameterString += "<tr><td>debugSensors:</td><td>" + simInfo.debugSensors + "</td></tr>";
  parameterString += "<tr><td>debugMouse:</td><td>" + simInfo.debugMouse + "</td></tr>";
  parameterString += "<tr><td>height:</td><td>" + simInfo.height + "</td></tr>";
  parameterString += "<tr><td>width:</td><td>" + simInfo.width + "</td></tr>";
  document.getElementById('ParameterTable').innerHTML = parameterString;
};

function convrgb(values) {
  return 'rgb(' + values.join(', ') + ')';
};


function rotate(robot, torque=0) {
  /* Apply a torque to the robot to rotate it.
   *
   * Parameters
   *   torque - rotational force to apply to the body.
   *            Try values around +/- 0.005.
   *            Don't pass Infinity, or robot will leave the arena.
   */
  robot.body.torque = torque;
};

function drive(robot, force=0) {
  /* Apply a force to the robot to move it.
   *
   * Parameters
   *   force - force to apply to the body.
   *           Try values around +/- 0.0005.
   *           Don't pass Infinity, or robot will leave the arena.
   */
  const orientation = robot.body.angle,
    force_vec = Matter.Vector.create(force, 0),
    move_vec = Matter.Vector.rotate(force_vec, orientation);
  Matter.Body.applyForce(robot.body, robot.body.position , move_vec);
};

function gaussNoise(sigma=1) {
  const x0 = 1.0 - Math.random();
  const x1 = 1.0 - Math.random();
  return sigma * Math.sqrt(-2 * Math.log(x0)) * Math.cos(2 * Math.PI * x1);
};

function senseColour(sensor) {
  var bodies = Matter.Composite.allBodies(simInfo.engine.world);
  // get only the bodies that are closest (and in range) of the sensor.
  var [_, bodies] = senseDistanceOfBodies(sensor, bodies);

  sensor.value = [0, 0, 0];
  for (let body of bodies) {
    for (var i = 0; i <= 2; i++) {
      // apply mild noise on the sensor reading, and clamp between valid values
      channel_value = Math.floor(body.color[i] + gaussNoise(3));
      channel_value = Matter.Common.clamp(channel_value, 0, 255);
      sensor.value[i] += channel_value / bodies.length;
    }
  }
}

function senseDistanceBoxes(sensor) {
  // filter bodies for boxes
  var bodies = Matter.Composite.allBodies(simInfo.engine.world).filter(function(body) {
    return body.role == "box"
  });
  var [rayLength, _] = senseDistanceOfBodies(sensor, bodies);

  // indicate if the sensor exceeded its maximum length by returning infinity
  if (rayLength > sensor.maxVal) {
    rayLength = Infinity;
  }
  else {
    // apply mild noise on the sensor reading, and clamp between valid values
    rayLength = Math.floor(rayLength + gaussNoise(3));
    rayLength = Matter.Common.clamp(rayLength, sensor.minVal, sensor.maxVal);
  }
  sensor.value = rayLength;
}

function senseDistanceWalls(sensor) {
  // filter bodies for walls
  var bodies = Matter.Composite.allBodies(simInfo.engine.world).filter(function(body) {
    return body.role == "wall" || body.role == "robot"
  });
  var [rayLength, _] = senseDistanceOfBodies(sensor, bodies);

  // indicate if the sensor exceeded its maximum length by returning infinity
  if (rayLength > sensor.maxVal) {
    rayLength = Infinity;
  }
  else {
    // apply mild noise on the sensor reading, and clamp between valid values
    rayLength = Math.floor(rayLength + gaussNoise(3));
    rayLength = Matter.Common.clamp(rayLength, sensor.minVal, sensor.maxVal);
  }
  sensor.value = rayLength;
}

function senseDistanceOfBodies(sensor, bodies) {
  /* Distance sensor simulation based on ray casting. Called from sensor
   * object, returns nothings the distance from the closest object(s) and
   * these objects.
   *
   * Idea: Cast a ray with a certain length from the sensor, and check
   *       via collision detection if objects intersect with the ray.
   *       To determine distance, run a Binary search on ray length.
   * Note: Sensor ray needs to ignore robot (parts), or start outside of it.
   *       The latter is easy with the current circular shape of the robots.
   * Note: Order of tests are optimized by starting with max ray length, and
   *       then only testing the maximal number of initially resulting objects.
   * Note: The sensor's "ray" could have any other (convex) shape;
   *       currently it's just a very thin rectangle.
   */

  const context = document.getElementById('arenaLemming').getContext('2d');

  const robotAngle = sensor.parent.body.angle,
    attachAngle = sensor.attachAngle,
    rayAngle = robotAngle + attachAngle + sensor.lookAngle;

  const rPos = sensor.parent.body.position,
    sRadius = sensor.attachRadius,
    startPoint = {x: rPos.x + (sRadius+1) * Math.cos(robotAngle + attachAngle),
      y: rPos.y + (sRadius+1) * Math.sin(robotAngle + attachAngle)};

  function getEndpoint(rayLength) {
    return {x: rPos.x + (sRadius + rayLength) * Math.cos(rayAngle),
      y: rPos.y + (sRadius + rayLength) * Math.sin(rayAngle)};
  };

  function sensorRay(bodies, rayLength) {
    // Cast ray of supplied length and return the bodies that collide with it.
    const rayWidth = 1e-100,
      endPoint = getEndpoint(rayLength);
    rayX = (endPoint.x + startPoint.x) / 2,
      rayY = (endPoint.y + startPoint.y) / 2,
      rayRect = Matter.Bodies.rectangle(rayX, rayY, rayLength, rayWidth,
        {isSensor: true, isStatic: true,
          angle: rayAngle, role: 'sensor'});

    var collidedBodies = [];
    for (var bb = 0; bb < bodies.length; bb++) {
      var body = bodies[bb];
      // coarse check on body boundaries, to increase performance:
      if (Matter.Bounds.overlaps(body.bounds, rayRect.bounds)) {
        for (var pp = body.parts.length === 1 ? 0 : 1; pp < body.parts.length; pp++) {
          var part = body.parts[pp];
          // finer, more costly check on actual geometry:
          if (Matter.Bounds.overlaps(part.bounds, rayRect.bounds)) {
            const collision = Matter.SAT.collides(part, rayRect);
            if (collision.collided) {
              collidedBodies.push(body);
              break;
            }
          }
        }
      }
    }
    return collidedBodies;
  };

  // call 1x with full length, and check all bodies in the world;
  // in subsequent calls, only check the bodies resulting here
  var rayLength = sensor.maxVal;
  bodies = sensorRay(bodies, rayLength);
  // if some collided, search for maximal ray length without collisions
  if (bodies.length > 0) {
    var lo = 0,
      hi = rayLength;
    while (lo < rayLength) {
      if (sensorRay(bodies, rayLength).length > 0) {
        hi = rayLength;
      }
      else {
        lo = rayLength;
      }
      rayLength = Math.floor(lo + (hi-lo)/2);
    }
  }
  // increase length to (barely) touch closest body (if any)
  rayLength += 1;
  bodies = sensorRay(bodies, rayLength);

  if (simInfo.debugSensors) {  // if invisible, check order of object drawing
    // draw the resulting ray
    endPoint = getEndpoint(rayLength);
    context.beginPath();
    context.moveTo(startPoint.x, startPoint.y);
    context.lineTo(endPoint.x, endPoint.y);
    context.strokeStyle = convrgb(sensor.color);
    context.lineWidth = 1.0;
    context.stroke();
    // mark all objects's lines intersecting with the ray
    for (var bb = 0; bb < bodies.length; bb++) {
      var vertices = bodies[bb].vertices;
      context.moveTo(vertices[0].x, vertices[0].y);
      for (var vv = 1; vv < vertices.length; vv += 1) {
        context.lineTo(vertices[vv].x, vertices[vv].y);
      }
      context.closePath();
    }
    context.stroke();
  }

  return [rayLength, bodies];
};

function rad2deg(x) {
  return 180 * x / Math.PI;
};

function deg2rad(x) {
  return Math.PI * x / 180;
};

function boundRadians(x) {
  return x % (2*Math.PI);
};

function senseRobotAngle(sensor) {
  /* Gyroscope sensor based on real angle, compromised with noise. Called with
   * sensor object, returns nothing, updates a new reading into sensor.value.
   *
   * Note that the Radian sensor values are not bounded; it simplifies some
   * operations, like rotating a given amount, and MatterJS is fine with it.
   * However, to print the values in an understandable format, they are bounded
   * between -2*PI and +2*PI, and converted to degrees.
   */

  const realRobotAngle = sensor.parent.body.angle;  // angle in radians

  // add some noise to the actual values to simulate an unbiased gyroscope
  function gaussNoise(sigma=Math.PI/45) {
    const x0 = 1.0 - Math.random();
    const x1 = 1.0 - Math.random();
    return sigma * Math.sqrt(-2 * Math.log(x0)) * Math.cos(2 * Math.PI * x1);
  };

  const noisyRobotAngle = realRobotAngle + gaussNoise();
  sensor.value = noisyRobotAngle;
  sensor.valueStr = format(rad2deg(boundRadians(noisyRobotAngle)));
};

function dragSensor(sensor, event) {
  const robotBay = document.getElementById('bayLemming'),
    bCenter = {x: robotBay.width/2,
      y: robotBay.height/2},
    rRadius = sensor.info.attachRadius,
    bScale = simInfo.bayScale,
    sSize = sensor.getWidth(),
    mAngle = Math.atan2(  event.mouse.x - bCenter.x,
      -(event.mouse.y - bCenter.y));
  sensor.info.attachAngle = mAngle;
  sensor.x = bCenter.x - sSize - bScale * rRadius * Math.sin(-mAngle);
  sensor.y = bCenter.y - sSize - bScale * rRadius * Math.cos( mAngle);
  repaintBay();
}

function loadSensor(sensor, event) {
  loadSensorInfo(sensor.sensor);
}

function loadSensorInfo(sensorInfo) {
  simInfo.baySensor = sensorInfo;
}

function loadBay(robot) {
  simInfo.bayRobot = robot;
  sensors = new Array();
  const robotBay = document.getElementById("bayLemming");
  const bCenter = {x: robotBay.width/2,
    y: robotBay.height/2},
    rSize = simInfo.robotSize,
    bScale = simInfo.bayScale;

  for (var ss = 0; ss < robot.info.sensors.length; ++ss) {
    const curSensor = robot.sensors[ss],
      attachAngle = curSensor.attachAngle;
    // put current sensor into global variable, make mouse-interactive
    sensors[ss] = makeInteractiveElement(new SensorGraphics(curSensor),
      document.getElementById("bayLemming"));
    const sSize = sensors[ss].getWidth();
    sensors[ss].x = bCenter.x - sSize - bScale * sSize * Math.sin(-attachAngle);
    sensors[ss].y = bCenter.y - sSize - bScale * sSize * Math.cos( attachAngle);
    sensors[ss].onDragging = dragSensor;
    sensors[ss].onDrag = loadSensor;
  }
  repaintBay();
}

function SensorGraphics(sensorInfo) {
  this.info = sensorInfo;
  this.plotSensor = plotSensor;
  // add functions getWidth/getHeight for graphics.js & mouse.js,
  // to enable dragging the sensor in the robot bay
  this.getWidth = function() { return 6; };
  this.getHeight = function() { return 6; };
}

function loadFromSVG() {
  var vertexSets = [];
  const svg = document.getElementById('robotbodySVG'),
    data = svg.contentDocument;

  jQuery(data).find('path').each(function(_, path) {
    var points = Matter.Svg.pathToVertices(path, 25);
    vertexSets.push(Matter.Vertices.scale(points, 0.2, 0.2));
  });

  return vertexSets;
};

function InstantiateRobot(robotInfo) {
  // load robot's body shape from SVG file
  const bodySVGpoints = loadFromSVG();
  this.body = Matter.Bodies.fromVertices(robotInfo.init.x,
    robotInfo.init.y,
    bodySVGpoints,
    {
      frictionAir: simInfo.airDrag,
      mass: simInfo.robotMass,
      color: robotInfo.color,
      role: 'robot'
    }, true);
  //var centre = Matter.Vertices.centre(this.body.vertices);
  //Matter.Vertices.translate(this.body.vertices, -centre, -1);

  Matter.World.add(simInfo.world, this.body);
  Matter.Body.setAngle(this.body, robotInfo.init.angle);

  // instantiate its sensors
  this.sensors = robotInfo.sensors;
  for (var ss = 0; ss < this.sensors.length; ++ss) {
    this.sensors[ss].parent = this;
  }

  // attach its helper functions
  this.rotate    = rotate;
  this.drive     = drive;
  this.info      = robotInfo;
  this.plotRobot = plotRobot;
  this.move      = robotMove;

  // add functions getWidth/getHeight for graphics.js & mouse.js,
  // to enable selection by clicking the robot in the arena
  this.getWidth  = function() { return 2 * simInfo.robotSize; };
  this.getHeight = function() { return 2 * simInfo.robotSize; };
}

function robotUpdateSensors(robot) {
  // update all sensors of robot; puts new values into sensor.value
  for (var ss = 0; ss < robot.sensors.length; ss++) {
    var sensor = robot.sensors[ss];
    sensor.sense(sensor);
  }
};

function getSensorValById(robot, id) {
  for (var ss = 0; ss < robot.sensors.length; ss++) {
    if (robot.sensors[ss].id == id) {
      return robot.sensors[ss].value;
    }
  }
  return undefined;  // if not returned yet, id doesn't exist
};

var BlockEnum = {
  NONE:  {value: 0, name: "None"},
  RED:   {value: 1, name: "Red"},
  GREEN: {value: 2, name: "Green"},
  BLUE:  {value: 3, name: "Blue"},
};

function blockInGripper(robot) {
  // FIXME: ugly code,
  // 1. we don't use getSensorValById because it somehow does not return arrays.
  // 2. the colour detection is very crude.
  let colourValue = undefined;
  for (var ss = 0; ss < robot.sensors.length; ss++) {
    if (robot.sensors[ss].id == "colour") {
      colourValue = robot.sensors[ss].value;
    }
  }
  if (colourValue == undefined) {
    return BlockEnum.NONE;
  }

  let colourLimit = 127;
  let noColourLimit = 20;
  // check if block is red
  if (
    colourValue[0] > colourLimit &&
    colourValue[1] < noColourLimit &&
    colourValue[2] < noColourLimit) {
    return BlockEnum.RED;
  }
  // check if block is blue
  if (
    colourValue[0] < noColourLimit &&
    colourValue[1] < noColourLimit &&
    colourValue[2] > colourLimit) {
    return BlockEnum.BLUE;
  }
  return BlockEnum.NONE;
}


function rotateBySetRadians(robot, set_radians=Math.PI/2, abs_torque=0.001) {
  /* Rotate the robot for the given amount in parameter set_radians;
   * abs_torque is the absolute amount of torque to apply per sim step.
   * Note that robotMove() is not executed until the rotation is done.
   *
   * Note how this is implemented by creating a closure to save the requested
   * goal angle when rotateBySetRadians() is called in robotMove().
   * The closure is temporarily inserted in robot.move (called each sim step).
   */
  abs_torque = Math.abs(abs_torque);  // never trust users :)

  // compute values to define and monitor the rotation progress
  const robot_angle = getSensorValById(robot, 'gyro'), // current robot angle
        goal_angle  = robot_angle + set_radians,       // goal angle after rotation
        error_angle = goal_angle - robot_angle,        // signed error angle
        sgn_torque  = Math.sign(error_angle);          // which direction to rotate

  function rotateUntilDone() {
    const error_angle = goal_angle - getSensorValById(robot, 'gyro');
    // anything left to rotate, or did we already overshoot (if < 0)?
    const error_remaining = error_angle * sgn_torque;

    if (error_remaining > 0) {  // not done yet, rotate a bit further
      robot.rotate(robot, sgn_torque*abs_torque);
    }
    else {  // done with rotation, call robotMove() again in next sim step
      robot.move = robotMove;
    }
  };

  return rotateUntilDone;
}


function robotMove(robot) {
  // By default, the Lemming should wander around in a slight right curve.
  //
  // If it senses a block its subsequent behavior depends on whether a block is
  // in the gripper and the block color.
  //   – If it carries no block it should drive straight towards it, and thus
  //     get the block into the gripper.
  //   - If it carries a blue block, it keeps wandering and ignores the detected
  //     block.
  //   – If it carries a red block it turns left to leave the block.
  //
  // If it senses a wall its behavior should depend on whether a block is in the
  // gripper and the block color.
  //   – If it carries no block it should turn either left or right.
  //   – If it carries a blue block it should turn left to leave the block.
  //   – If it carries a red block it should turn right to keep the block.
  //
  // TODO: prioritise walls over blocks? Or the other way round?
  const sensorDistWalls = getSensorValById(robot, 'distWalls'),
    sensorDistBoxes = getSensorValById(robot, 'distBoxes');

  let sensesBlock = isFinite(sensorDistBoxes),
    sensesWall  = isFinite(sensorDistWalls),
    block = blockInGripper(robot);

  if (sensesWall) {
    // If it senses a wall its behavior should depend on whether a block is in
    // the gripper and the block color.
    if (block == BlockEnum.NONE) {
      //   – If it carries no block it should turn either left or right.
      // FIXME: random?
      robot.move = rotateBySetRadians(robot, spinAngleDump, spinTorqueDump);
    } else if (block == BlockEnum.BLUE) {
      //   – If it carries a blue block it should turn left to leave the block.
      robot.move = rotateBySetRadians(robot, spinAngleDump, spinTorqueDump);
    } else if (block == BlockEnum.RED) {
      //   – If it carries a red block it should turn right to keep the block.
      robot.move = rotateBySetRadians(robot, spinAngleKeep, spinTorqueKeep);
    } else {
      console.log("unknown blocktype: " + block);
    }
  } else if (sensesBlock) {
    // If it senses a block its subsequent behavior depends on whether a block is
    // in the gripper and the block color.
    if (block == BlockEnum.NONE) {
      //   – If it carries no block it should drive straight towards it, and thus
      //     get the block into the gripper.
      robot.drive(robot, driveSpeed);
    } else if (block == BlockEnum.BLUE) {
      //   - If it carries a blue block, it keeps wandering and ignores the
      //     detected block.
      robot.rotate(robot, wanderTorque);
      robot.drive(robot, driveSpeed);
    } else if (block == BlockEnum.RED) {
      //   – If it carries a red block it turns left to leave the block.
      robot.move = rotateBySetRadians(robot, spinAngleDump, spinTorqueDump);
    } else {
      console.log("unknown blocktype: " + block);
    }
  } else {
    // By default, the Lemming should wander around in a slight right curve.
    robot.rotate(robot, wanderTorque);
    robot.drive(robot, driveSpeed);
  }
};

function plotSensor(context, x=this.x, y=this.y) {
  var color = 'black';
  if (this.info.color) {
    color = convrgb(this.info.color);
  }
  context.beginPath();
  context.arc(x + this.getWidth()/2,
    y + this.getHeight()/2,
    this.getWidth()/2, 0, 2*Math.PI);
  context.closePath();
  context.fillStyle = color;
  context.strokeStyle = color;
  context.fill();
  context.stroke();
}

function plotRobot(context,
  xTopLeft = this.body.position.x,
  yTopLeft = this.body.position.y) {
  var x, y, scale, angle, i, half, full,
    rSize = simInfo.robotSize;
  const showInternalEdges = false;

  if (context.canvas.id == "bayLemming") {
    scale = simInfo.bayScale;
    half = Math.floor(rSize/2*scale);
    full = half * 2;
    x = xTopLeft + full;
    y = yTopLeft + full;
    angle = -Math.PI / 2;
  } else {
    scale = 1;
    half = Math.floor(rSize/2*scale);
    full = half * 2;
    x = xTopLeft;
    y = yTopLeft;
    angle = this.body.angle;
  }
  context.save();
  context.translate(x, y);
  context.rotate(angle);

  if (context.canvas.id == "arenaLemming") {
    // draw into world canvas without transformations,
    // because MatterJS thinks in world coords...
    context.restore();

    const body = this.body;
    // handle compound parts

    context.beginPath();
    for (k = body.parts.length > 1 ? 1 : 0; k < body.parts.length; k++) {
      part = body.parts[k];
      context.moveTo(part.vertices[0].x,
        part.vertices[0].y);
      for (j = 1; j < part.vertices.length; j++) {
        if (!part.vertices[j - 1].isInternal || showInternalEdges) {
          context.lineTo(part.vertices[j].x,
            part.vertices[j].y);
        } else {
          context.moveTo(part.vertices[j].x,
            part.vertices[j].y);
        }

        if (part.vertices[j].isInternal && !showInternalEdges) {
          context.moveTo(part.vertices[(j + 1) % part.vertices.length].x,
            part.vertices[(j + 1) % part.vertices.length].y);
        }
      }
      context.lineTo(part.vertices[0].x,
        part.vertices[0].y);
    }
    context.strokeStyle = convrgb(body.color);
    context.lineWidth = 1.5;
    context.stroke();

    // to draw the rest, rotate & translate again
    context.save();
    context.translate(x, y);
    context.rotate(angle);
  }

  // Plot sensor positions into world canvas.
  if (context.canvas.id == "arenaLemming") {
    for (ss = 0; ss < this.info.sensors.length; ++ss) {
      const attachAngle = this.info.sensors[ss].attachAngle,
        attachRadius = this.info.sensors[ss].attachRadius;
      var color = 'black';

      if (this.info.sensors[ss].color) {
        color = convrgb(this.info.sensors[ss].color);
      }

      context.beginPath();
      context.arc(attachRadius * Math.cos(this.info.sensors[ss].attachAngle),
        attachRadius * Math.sin(this.info.sensors[ss].attachAngle),
        scale, 0, 2*Math.PI);
      context.closePath();
      context.fillStyle = color;
      context.strokeStyle = color;
      context.fill();
      context.stroke();
    }
  }
  context.restore();
}

function arraysEqual(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length != b.length) return false;
  for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
}

function closeToWall(box) {
  var bs = simInfo.boxSize,
      fw = simInfo.width,
      fh = simInfo.height;
  var x = box.position.x,
      y = box.position.y;
  if (x <=  0 + 5*bs ||
      x >= fw - 5*bs || 
      y <=  0 + 5*bs || 
      y >= fh - 5*bs) {
    if (arraysEqual(box.color, [0, 0, 200])) {
      simInfo.blueBoxesCloseToWall++;
    } else if (arraysEqual(box.color, [200, 0, 0])) {
      simInfo.redBoxesCloseToWall++;
    }
  }
}

function checkIfDone() {
  var boxes = Matter.Composite.allBodies(simInfo.engine.world).filter(function(body) {
    return body.role == "box";
  });
  simInfo.blueBoxesCloseToWall = 0;
  simInfo.redBoxesCloseToWall = 0;

  for (var b = 0; b < boxes.length; ++b) {
    closeToWall(boxes[b]);
  }

  return simInfo.blueBoxesCloseToWall == nrOfBlueBoxes;
}

function simStep() {
  if (checkIfDone()) {
    toggleSimulation();
  }
  // advance simulation by one step (except MatterJS engine's physics)
  else if (simInfo.curSteps < simInfo.maxSteps) {
    repaintBay();
    drawBoard();
    for (var rr = 0; rr < robots.length; ++rr) {
      var robot = robots[rr];
      robotUpdateSensors(robot);
      robot.move(robot);
      // To enable selection by clicking (via mouse.js/graphics.js),
      // the position on the canvas needs to be defined in (x, y):
      const rSize = simInfo.robotSize;
      robot.x = robot.body.position.x - rSize;
      robot.y = robot.body.position.y - rSize;
    }
    // count and display number of steps
    simInfo.curSteps += 1;
    document.getElementById("SimStepLabel").innerHTML =
      padnumber(simInfo.curSteps, 5) +
      ' of ' + padnumber(simInfo.maxSteps, 5);
    document.getElementById("BlueBoxesCloseToWallLabel").innerHTML =
      padnumber(simInfo.blueBoxesCloseToWall, 2) +
      ' of ' + nrOfBlueBoxes;
    document.getElementById("RedBoxesCloseToWallLabel").innerHTML =
      padnumber(simInfo.redBoxesCloseToWall, 2) +
      ' of ' + nrOfRedBoxes;
  }
  else {
    toggleSimulation();
  }
}

function drawBoard() {
  var context = document.getElementById('arenaLemming').getContext('2d');
  context.fillStyle = "#444444";
  context.fillRect(0, 0, simInfo.width, simInfo.height);

  // draw objects within world
  const Composite = Matter.Composite,
    bodies = Composite.allBodies(simInfo.world);

  for (var bb = 0; bb < bodies.length; bb += 1) {
    var vertices = bodies[bb].vertices,
      vv;

    // draw all non-robot bodies here (walls and boxes)
    // don't draw robot's bodies here; they're drawn in plotRobot()
    if (bodies[bb].role != 'robot') {
      context.beginPath();
      context.moveTo(vertices[0].x, vertices[0].y);
      for (vv = 1; vv < vertices.length; vv += 1) {
        context.lineTo(vertices[vv].x, vertices[vv].y);
      }
      if (bodies[bb].color) {
        context.strokeStyle = convrgb(bodies[bb].color);
        context.closePath();
        context.stroke();
      }
    }
  }
  context.lineWidth = 1;

  // draw all robots
  for (var rr = 0; rr < robots.length; ++rr) {
    robots[rr].plotRobot(context);
  }
}

function repaintBay() {
  // update inset canvas showing information about selected robot
  const robotBay = document.getElementById('bayLemming'),
    context = robotBay.getContext('2d');
  context.clearRect(0, 0, robotBay.width, robotBay.height);
  simInfo.bayRobot.plotRobot(context, 10, 10);
  for (var ss = 0; ss < sensors.length; ss++) {
    sensors[ss].plotSensor(context);
  }

  // print sensor values of selected robot next to canvas
  if (!(simInfo.curSteps % 5)) {  // update slow enough to read
    var sensorString = '';
    const rsensors = simInfo.bayRobot.sensors;
    for (ss = 0; ss < rsensors.length; ss++) {
      sensorString += "<tr><td>" + rsensors[ss].id + ":</td>" + "<td><b>";
      sensorString += rsensors[ss].value + "</b></td></tr>";
    }
    sensorString += "<tr><td>block gripper: </td><td><b>" + blockInGripper(robots[0]).name + "</b></td></tr>";
    document.getElementById('SensorLabel').innerHTML = sensorString;
  }
}

function setRobotNumber(newValue) {
  var n;
  while (robots.length > newValue) {
    n = robots.length - 1;
    Matter.World.remove(simInfo.world, robots[n].body);
    robots[n] = null;
    robots.length = n;
  }

  while (robots.length < newValue) {
    if (newValue > RobotInfo.length) {
      console.warn('You request '+newValue+' robots, but only ' + RobotInfo.length +
        ' are defined in RobotInfo!');
      toggleSimulation();
      return;
    }
    n = robots.length;
    robots[n] = makeInteractiveElement(new InstantiateRobot(RobotInfo[n]),
      document.getElementById("arenaLemming"));

    robots[n].onDrop = function(robot, event) {
      robot.isDragged = false;
    };

    robots[n].onDrag = function(robot, event) {
      robot.isDragged = true;
      loadBay(robot);
      return true;
    };
  }
}


function padnumber(number, size) {
  if (number == Infinity) {
    return 'inf';
  }
  const s = "000000" + number;
  return s.substr(s.length - size);
}

function format(number, numFix=1) {
  // prevent HTML elements to jump around at sign flips etc
  return (number >= 0 ? '+' : '−') + Math.abs(number).toFixed(numFix);
}

function toggleSimulation() {
  simInfo.doContinue = !simInfo.doContinue;
  if (simInfo.doContinue) {
    Matter.Runner.start(simInfo.runner, simInfo.engine);
  }
  else {
    Matter.Runner.stop(simInfo.runner);
  }
}

